CC = clang
CFLAGS = -Wall
OUTPUT = shell
OBJFILES = shell.o tokenizer.o tree.o parser.o list.o evaluator.o history.o common.o

.PHONY: $(OUTPUT)
$(OUTPUT): $(OBJFILES)
	$(CC) $(CFLAGS) $(OBJFILES) -o $(OUTPUT)

.PHONY: debug
debug: CFLAGS += -DDEBUG
debug: $(OUTPUT)

parser_test: $(OBJFILES)
	$(CC) $(CFLAGS) $(OBJFILES) -o parser_test

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: test
test:
	$(CC) $(FLAGS) parser_test.c parser.c tree.c tokenizer.c list.c -o parser_test
	$(CC) $(FLAGS) tokenizer_test.c tokenizer.c -o tokenizer_test
	$(CC) $(FLAGS) history_test.c history.c list.c -o history_test
	$(CC) $(FLAGS) echo.c -o echo
	$(CC) $(FLAGS) spin.c -o spin

.PHONY: run-test
run-test:
	chmod 777 ./all_test.sh
	./all_test.sh


.PHONY: clean
clean:
	rm -f *.o $(OUTPUT) parser_test tokenizer_test history_test core*

.PHONY: all
all: shell test
