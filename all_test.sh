echo "Running testsuite for shell"
echo "Running unit tests"
echo "Test parser"
./parser_test
echo "Test tokenizer"
./tokenizer_test
echo "Test history"
./history_test
echo "Running e2e tests"
chmod 777 e2e.sh
bash e2e.sh
