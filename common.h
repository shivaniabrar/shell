#ifndef COMMON_H
#define COMMON_H

typedef enum bool {
    false, true, 
} bool;

typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);

//#define DEBUG

#ifdef DEBUG
#define LOG(...) \
do { \
printf(__VA_ARGS__); \
} while(0)
#else
#define LOG(...)
#endif

#endif