#!/usr/pkg/bin/bash

function pgrep() {
    ps -e | grep "$1" | grep -v grep
}

function waitjobs() {
    for job in `jobs -p`
do
    wait $job
done
}

failedtestcases=0

echo "Running e2e tests for shell"
echo -n "Shell Prompt (Should be current working directory): "
./shell < prompt.txt | grep -q $PWD
if [ $? -eq 0 ]
then
	echo Success
else
	echo Failure
	let "failedtestcases++"
fi

echo -n "Test commands in different paths (Should be able to \
run commands located in PATH irrespective of current directory): "
./shell < different_paths.txt | grep -q $PWD
if [ $? -eq 0 ]
then
	echo Success
else
	echo Failure
	let "failedtestcases++"
fi


echo -n "Sequence Operator Tests "
test="$(head -1 seq1.txt)"
echo -n "($test): "
./shell < seq1.txt | grep -q '12'
if [ $? -eq 0 ]
then
	echo Success
else
	echo Failure
	let "failedtestcases+=1"
fi

echo -n "Multiple Sequence Operator Tests "
test="$(head -1 seq2.txt)"
echo -n "($test): "
./shell < seq2.txt | grep -q '123'
if [ $? -eq 0 ]
then
	echo Success
else
	echo Failure
	let "failedtestcases+=1"
fi

echo -n "Parallel Operator Tests"
test="$(head -1 testpar1.txt)"
echo -n "($test): "
./shell < testpar1.txt > /dev/null &
sleep 1
if pgrep "spin 5" > /dev/null
then
	echo Success
else
	echo Failure
	let "failedtestcases+=1"
fi
if pgrep "spin 4" > /dev/null
then
	echo Success
else
	echo Failure
	let "failedtestcases+=1"
fi
if pgrep "spin 3" > /dev/null
then
	echo Success
else
	echo Failure
	let "failedtestcases+=1"
fi
echo Waiting for jobs to finish...
waitjobs

echo -n "Sequence and Parallel Operator Tests "
test="$(head -1 testparseq1.txt)"
echo -n "($test): "
./shell < testparseq1.txt | grep -qE '123|132'
if [ $? -eq 0 ]
then
	echo Success
else
	echo Failure
	let "failedtestcases+=1"
fi


