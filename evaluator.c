//
// Created by Abrar Shivani on 2/23/17.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "tokenizer.h"
#include "Evaluator.h"
#include "tree.h"
#include "common.h"
#include "shell.h"
#include "parser.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/stat.h>

int alarm_feature = DEFAULT_ALARM_FEATURE;
volatile sig_atomic_t alarm_fired = 0;
pid_t executor_process_group = 0;

void  sigalrm_handler(int sig)
{
    alarm_fired = 1;
}

void sigint_handler(int sig)
{
    if (executor_process_group != 0) {
        kill(executor_process_group, SIGKILL);
    }
}


void
sigquit_handler(int sig)
{
    if (executor_process_group != 0) {
        kill(executor_process_group, SIGKILL);
    }
    printf("SIGQUIT signal received ... terminating shell.\n");
    exit(1);
}


bool
Evaluator(Tree *tree)
{
    int evaluator_process = -1;
    char usr_input[MAX_CHAR_FOR_CMD_LINE];
    LOG("Compile");
    Node *root = NULL;
    if (tree == NULL) {
        return 0;
    }
    root = GetRoot(tree);
    if ((evaluator_process = fork()) == 0) {
        setpgid(0, 0);
        Signal(SIGALRM, SIG_DFL);
        Signal(SIGINT,  SIG_DFL);
        Signal(SIGQUIT,  SIG_DFL);
        Evaluate(root);
        exit(0);
    }

    executor_process_group = evaluator_process * -1;
    if (alarm_feature == ON) {
        alarm(ALARM_TIME);
    }
    while(waitpid(evaluator_process, NULL, 0) == -1) {
        if (alarm_fired == 1) {
            printf("\nDo you want to exit (pid = %d) ?(Y/N): ", getpid());
            fgets(usr_input, MAX_CHAR_FOR_CMD_LINE, stdin);
            if ((strlen(usr_input)>0) && (usr_input[strlen(usr_input) - 1] == '\n'))
                usr_input[strlen (usr_input) - 1] = '\0';
            printf("\n");
            if (usr_input[0] == 'Y') {
                kill(executor_process_group, SIGKILL);
            } else {
                printf("Resetting alarm\n");
                alarm_fired = 0;
                alarm(ALARM_TIME);
            }
        }
    }
    alarm(0);
    executor_process_group = 0;
    LOG("Back to parent");
    return true;
}

bool
Evaluate(Node *root) {
    pid_t child_process = -1;
    Node *node_child = NULL;
    if (root == NULL) {
        return false;
    }

    if (root->operator == SEQUENTIAL) {
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            if ((child_process = fork()) == 0) {
                return Evaluate(node_child);
            } else {
                waitpid(child_process, NULL, 0);
            }
        }
    } else if (root->operator == PARALLEL) {
        {
            FOR_EACH(Node, sibling, (&(root->child)), node_child)
                if ((child_process = fork()) == 0) {
                    return Evaluate(node_child);
                }
                node_child->pid = child_process;
            }
        }
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            waitpid(node_child->pid, NULL, 0);
        }
    } else if (root->operator == NOOP) {
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            return Evaluate(node_child);
        }

    } else if (root->operator == COMMAND) {
        execute(root->value, false);
    } else {
        LOG("Unknown Operator: %u\n", root->operator);
    }

    return true;
}

int
execute(char *cmdline, bool builtin)
{
    int argc = 0;
    int isbuiltin = 0;
    char *cmd = NULL;
    char *extra_argv = NULL;
    char *argv[MAX_ARGUMENTS];
    if (builtin == true) {
        cmd = strdup(cmdline);
    } else {
        cmd = cmdline;
    }
    argv[0] = strtok(cmd, " \n\t");
    while (argc < MAX_ARGUMENTS && argv[argc] != NULL) {
        ++argc;
        argv[argc] = strtok(NULL, " \n\t");
    }
    LOG("%s: Command.\n", argv[0]);

    if(argv[argc] != NULL) {
        extra_argv = strtok(NULL, " \n\t");
        if (extra_argv != NULL) {
            if (builtin == true) {
                free(cmd);
            }
            return -1;
        }
    }

    if (builtin == true) {
        optind = 1;
        isbuiltin = built_in_cmd(argc, argv);
        free(cmd);
        return isbuiltin;
    } else {
        if (execvp(argv[0], argv) < 0) {
            printf("%s: Command not found.\n", argv[0]);
            return 0;
        }
    }
    return 0;
}

int
built_in_cmd(int argc, char **argv)
{
    if (argc == 0 || argv == NULL) {
        return 1;
    }
    if (!strcmp(argv[0], "quit")) {
        execute_quit(argc, argv);
    } else if (!strcmp(argv[0], "set")) {
        execute_set(argc, argv);
    } else if (!strcmp(argv[0], "cd")) {
        execute_cd(argc, argv);
    } else {
        return 0;
    }
    return 1;
}

int
execute_cd(int argc, char **argv) {
    if (argc > 2 || argc == 1) {
        printf("Usage: cd <path>\n");
        return 1;
    }
    if (chdir(argv[1]) != 0) {
        perror(argv[1]);
        return 1;
    }
    return 0;
}


int
execute_quit(int argc, char **argv) {
    if (argc > 1) {
        printf("Usage: quit");
        return 1;
    }
    exit(0);
}

int
execute_set(int argc, char **argv) {
    int opt = 0;
    LOG("set: %d %s", argc, argv[2]);
    while ((opt = getopt(argc, argv, "p:h:a:")) != -1)
    {
        LOG("set: %d %s", argc, argv[2]);
        switch (opt)
        {
            case 'p':
                setenv("PATH", optarg, 1);
                LOG("PATH = %s", getenv("PATH"));
                break;
            case 'h':
                if (IsDirectory(optarg) == 0) {
                    printf("%s: Cannot find directory\n", optarg);
                    break;
                }
                setenv("HOME", optarg, 1);
                break;
            case 'a':
                if (!strcmp(optarg, "on")) {
                    alarm_feature = ON;
                    break;
                } else if (!strcmp(optarg, "off")) {
                    alarm_feature = OFF;
                    break;
                }
            default:
                printf("Usage: set -p path -h home -a alarm [off|on]");
                return 1;
        }
    }
    return 0;
}

int IsDirectory(const char *path) {
    struct stat statbuf;
    if (stat(path, &statbuf) != 0)
        return 0;
    return S_ISDIR(statbuf.st_mode);
}


