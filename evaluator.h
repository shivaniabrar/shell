//
// Created by Abrar Shivani on 2/25/17.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "tokenizer.h"
#include "tree.h"
#include "common.h"
#include "shell.h"
#include "parser.h"
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifndef EVALUATOR_H
#define EVALUATOR_H

#define ALARM_TIME 3
#define DEFAULT_ALARM_FEATURE ON

#define OFF 0
#define ON 1

void  sigalrm_handler(int sig);
void sigint_handler(int sig);
void sigquit_handler(int sig);
bool Evaluator(Tree *tree);
bool Evaluate(Node *root);


int execute(char *cmdline, bool builtin);
int built_in_cmd(int argc, char **argv);
int execute_quit(int argc, char **argv);
int execute_set(int argc, char **argv);
int execute_cd(int argc, char **argv);
int IsDirectory(const char *path);

#endif //EVALUATOR_H
