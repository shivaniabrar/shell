#include <string.h>
#include <stdlib.h>

#include "history.h"

CMDNode *
AllocateAndCreateCMDNode(char *command) {
     CMDNode *cmdnode = NULL;
     char *buffer = NULL;

     if (command == NULL) {
         return cmdnode;
     }

     // Allocate CMDNode and buffer
     cmdnode = malloc(sizeof(CMDNode));
     buffer = malloc((strlen(command) + 1) * sizeof(char));

     // Initialize CMDNode
     strcpy(buffer, command);
     cmdnode->command = buffer;
     return cmdnode;
}
 
void
FreeCMDNode(CMDNode *cmdnode) {
     if (cmdnode->command != NULL) {
         free(cmdnode->command);
     }

     if (cmdnode != NULL) {
         free(cmdnode);
     }
}

char *
GetCmd(CMDNode *cmdnode) {
    if (cmdnode != NULL && cmdnode->command != NULL) {
        return cmdnode->command;
    }
    return NULL;
}

Token *
CreateToken(History *history, CMDNode *cmdnode, char *prefix) {
     Token *token = NULL;
     char *buffer = NULL;

     if (history == NULL) {
         return token;
     }

     // Allocate token
     token = malloc(sizeof(Token));

     // Allocate buffer
     buffer = malloc((strlen(prefix) + 1) * sizeof(char));


     strcpy(buffer, prefix);

     // Initialize token
     token->cmdnode = cmdnode;
     token->history = history;
     token->prefix = buffer;
     return token;
}

bool
GetCmdFromToken(Token *token, char *buffer) {
    char *cmd = NULL;
    if (token == NULL || token->cmdnode == NULL || buffer == NULL) {
        return false;
    }
    cmd = GetCmd(token->cmdnode);
    if (cmd == NULL) {
        return false;
    }
    bzero(buffer, strlen(buffer));
    strcpy(buffer, cmd);
    return true;
}

void
FreeToken(Token *token) {
     if (token == NULL || token->prefix == NULL) {
         return;
     }
     free(token->prefix);
}

bool
InitHistory(History *history, char *filename) {
    char *line = NULL;

    if (history == NULL || filename == NULL) {
        return false;
    }

    InitListNode(&history->listHead);

    strcpy(history->filename, filename);
    history->fp = fopen(history->filename, "r+");

    if (history->fp == NULL) {
        history->fp = fopen(history->filename, "w+");
    }

    line = malloc(MAX_CHAR_FOR_CMD_LINE * sizeof(char));


    while (fgets(line, MAX_CHAR_FOR_CMD_LINE, history->fp) != NULL) {
        line[strcspn(line, "\n")] = 0;
        _AddToHistory(history, line);
    }

    free(line);
    return true;
}

bool
AddToHistory(History *history, char *command) {
    if (history == NULL || command == NULL) {
        return false;
    }
     _AddToHistory(history, command);
    fprintf(history->fp, "%s\n", command);
    return true;
}

void 
_AddToHistory(History *history, char *command) {
    CMDNode *cmdnode = AllocateAndCreateCMDNode(command);
    AddToFront(&history->listHead, &cmdnode->listLink);
}

bool 
IsHistoryEmpty(History *history) {
    if (history == NULL) {
        return false;
    }
    return IsEmpty(&history->listHead);
}

void
CloseHistory(History *history) {
    CMDNode *cmdnode = NULL;
    if (history == NULL) {
        return;
    }
    fflush(history->fp);
    fclose(history->fp);
    FOR_EACH(CMDNode, listLink, (&history->listHead), cmdnode)
        free(cmdnode);
    }
}


Token *
AutoComplete(History *history, char *prefix) {
    bool result = false;
    Token *token = NULL;

    if (history == NULL || prefix == NULL) {
        return NULL;
    }

    token = CreateToken(history, NULL, prefix);

    result = SearchUp(token);
    if (result == true) {
         return token;
    }
    FreeToken(token);
    return NULL;
}

bool
SearchUp(Token *token) {    
    ListNode *startnode = NULL;
    
    if (token == NULL || token->history == NULL) {
        return false;
    }
    
    if (token->prefix == NULL || strlen(token->prefix) == 0) {
        return false;
    }

    startnode = &(token->history->listHead);
    if (token->cmdnode != NULL) {
        startnode = &(token->cmdnode->listLink);
    }

    FOR_EACH_FROM_NODE(CMDNode, listLink, (&(token->history->listHead)), startnode, token->cmdnode)
       if ((strlen(token->cmdnode->command) !=  strlen(token->prefix)) && (strncmp(token->cmdnode->command, token->prefix, strlen(token->prefix)) == 0)) {
            return true;
       }
    }
    return false;
}

bool 
SearchDown(Token *token) {
    ListNode *startnode = NULL;
    
    if (token == NULL || token->history == NULL) {
        return false;
    }

    if (token->prefix == NULL || strlen(token->prefix) == 0) {
        return false;
    }

    startnode = &(token->history->listHead);
    if (token->cmdnode != NULL) {
        startnode = &(token->cmdnode->listLink);
        
    }

    FOR_EACH_FROM_NODE_PREV(CMDNode, listLink, (&(token->history->listHead)), startnode, token->cmdnode)
       if ((strlen(token->cmdnode->command) !=  strlen(token->prefix)) && (strncmp(token->cmdnode->command, token->prefix, strlen(token->prefix)) == 0)) {
            return true;
       }
    }
    return false;
}

