#include <stdio.h>

#include "list.h"
#include "shell.h"

#define FILENAME 100
#ifndef HISTORY_H
#define HISTORY_H

/************************************************************
 *                    Data structures                       *
 ************************************************************/
 
typedef struct History {
    char filename[FILENAME];
    ListNode listHead;
    FILE *fp;
}History;

typedef struct CMDNode {
    char *command;
    ListNode listLink;
} CMDNode;

typedef struct Token {
    CMDNode *cmdnode;
    History *history;
    char *prefix;
} Token;

/************************************************************
 *                    Interface for History                 *
 ************************************************************/
bool InitHistory(History *history, char *filename);
bool AddToHistory(History *history, char *command);
void _AddToHistory(History *history, char *command);
void CloseHistory(History *history);

Token *AutoComplete(History *history, char *prefix);
bool SearchUp(Token *token);
bool SearchDown(Token *token);
bool IsHistoryEmpty(History *history);


/************************************************************
 *                    Interface for CMD                      *
 ************************************************************/
CMDNode *AllocateAndCreateCMDNode(char *command);
char *GetCmd(CMDNode *cmdnode);
void FreeCMDNode(CMDNode *);

/************************************************************
 *                    Interface for Token                     *
 ************************************************************/
Token *CreateToken(History *history, CMDNode *cmdnode, char *prefix);
bool GetCmdFromToken(Token *token, char *buffer);
void FreeToken(Token *token);
 
#endif