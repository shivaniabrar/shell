//
// Created by Abrar Shivani on 2/26/17.
//

#include <stdio.h>
#include "history.h"
#include "framework_api.h"
#include "framework.h"
// test name
char *testName;

/* test output files */
#define TESTPF "test_pagefile.bin"

/* prototypes for test functions */
static void testInitHistory();
static void testHistoryIsEmpty();
static void testAutoComplete();
static void testSearchUpAndDown();

int
main(int argc, char **argv) {

    testName = "";

    testInitHistory();
    testHistoryIsEmpty();
    testAutoComplete();
    testSearchUpAndDown();
    return 0;
}

void
testInitHistory()
{
    History history;
    testName = "test able to initialize history";
    ASSERT_TRUE(InitHistory(&history, "htest"), "History Initialization");
    TEST_DONE();
}

void
testHistoryIsEmpty()
{
    History history;
    testName = "test check empty history file";
    InitHistory(&history, "emptyfile");
    ASSERT_TRUE(IsHistoryEmpty(&history), "History is empty check verified");
    TEST_DONE();
}

void
testAutoComplete()
{
    History history;
    Token *token = NULL;
    char buf[100];
    char *expected = "ls";
    testName = "test autocomplete feature";
    InitHistory(&history, "htest");
    ASSERT_TRUE(AddToHistory(&history, expected), "Should be able to add to history");
    token = AutoComplete(&history, "l");
    ASSERT_TRUE(token, "Command should be there in history");
    ASSERT_TRUE(GetCmdFromToken(token, buf), "Should be able to get command from token");
    ASSERT_EQUALS_STRING(expected, buf, "Autocomplete verified");
    TEST_DONE();
}

void
testSearchUpAndDown()
{
    History history;
    Token *token = NULL;
    char buf[100];
    char *expected = "ls -l";
    char *expected1 = "ls";
    testName = "test search up and down feature";
    InitHistory(&history, "htest");
    ASSERT_TRUE(AddToHistory(&history, expected), "Add ls -l to history ");
    ASSERT_TRUE(AddToHistory(&history, expected1), "Add ls to history");
    token = AutoComplete(&history, "l");
    ASSERT_TRUE(token, "Command should be there in history");
    ASSERT_TRUE(GetCmdFromToken(token, buf), "Should be able to get command from token");
    ASSERT_EQUALS_STRING(expected1, buf, "Autocomplete");
    ASSERT_TRUE(SearchUp(token), "SearchUp Validation");
    ASSERT_TRUE(GetCmdFromToken(token, buf), "Should be able to get command from token");
    ASSERT_EQUALS_STRING(expected, buf, "SearchUp");
    ASSERT_TRUE(SearchDown(token), "SearchDown");
    ASSERT_TRUE(GetCmdFromToken(token, buf), "Should be able to get command from token");
    ASSERT_EQUALS_STRING(expected1, buf, "SearchDown Validation");
    TEST_DONE();
}