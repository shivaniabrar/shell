#include "list.h"

void InitListNode(ListNode * node)
{
    node->prev = node;
    node->next = node;
}

void AddToFront(ListNode *head, ListNode *node)
{
    node->next = head->next;
    node->prev = head;
    head->next->prev = node;
    head->next = node;
}

void AddToBack(ListNode *head, ListNode *node)
{
    node->next = head;
    node->prev = head->prev;
    head->prev->next = node;
    head->prev = node;
}

void RemoveFromList(ListNode *node)
{
    node->prev->next = node->next;
    node->next->prev = node->prev;
    InitListNode(node);
}

bool IsEmpty(ListNode* head){
    if (head->next==head && head->prev==head)
    {
        return true;
    }
    return false;
}