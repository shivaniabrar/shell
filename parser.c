//
// Created by Abrar Shivani on 2/23/17.
//

#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "parser.h"
#include "tokenizer.h"
#include "tree.h"
#include "common.h"

char *delims = ";&()";

Tree *
GetParseTree(char *command)
{
    Tokenizer *tokenizer = CreateTokenizer(command);
    Tree *tree = CreateTree();
    bool result = true;
    while(GetNextToken(tokenizer, delims) != NULL && result == true) {
        tokenizer->token = trimws(tokenizer->token);
        LOG("Tokens %c: %s\n", tokenizer->delim, tokenizer->token);
        result = AddTokenToTree(tree, tokenizer);
    }
    DestroyTokenizer(tokenizer);
    if (result == false || GetParent(tree) != GetRoot(tree)) {
        if (GetParent(tree) != GetRoot(tree)) {
            printf("Mismatched parenthesis\n");
        }
        DestroyTree(tree);
        return NULL;
    }
    return tree;
}

bool
AddTokenToTree(Tree *tree, Tokenizer *tokenizer)
{
    Node *node = NULL;
    if (tree == NULL || tokenizer == NULL) {
        return false;
    }

    if (tokenizer->delim == '&') {
         if (GetParent(tree) == NULL) {
             return false;
         }
         if(GetParent(tree)->operator == NOOP) {
             ChangeNOOPToOtherOperator(GetParent(tree), PARALLEL);
         } else if (GetParent(tree)->operator != PARALLEL) {
             return false;
         }
    }

    if (tokenizer->delim == ';') {
        if (GetParent(tree) == NULL) {
            return false;
        }
        if(GetParent(tree)->operator == NOOP) {
            ChangeNOOPToOtherOperator(GetParent(tree), SEQUENTIAL);
        } else if (GetParent(tree)->operator != SEQUENTIAL) {
            return false;
        }
    }

    if (tokenizer->token != NULL && strlen(tokenizer->token) != 0){
        if (CheckAndCreateChild(tree, COMMAND, tokenizer->token) == NULL) {
            return false;
        }
    }

    if (tokenizer->delim == '(') {
        node = CheckAndCreateChild(tree, NOOP, NULL);
        if(node == NULL) {
            return false;
        }
        SetParent(tree, node);
    }


    if (tokenizer->delim == ')') {
        if(GetParent(tree) == NULL){
            return false;
        }
        if (GetParent(tree)->parent == NULL) {
            printf("Mismatched parenthesis\n");
            return false;
        }
        SetParent(tree, GetParent(tree)->parent);
    }

    return true;
}

char *
trimws(char *str)
{
    char *end;

    while (isspace((unsigned char)*str)) {
        str++;
    }
    if (*str == '\0') {
        return str;
    }

    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char)*end)) {
        end--;
    }

    *(end + 1) = '\0';
    return str;
}
