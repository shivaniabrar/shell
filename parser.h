//
// Created by Abrar Shivani on 2/24/17.
//

#include "tokenizer.h"
#include "tree.h"

#ifndef PARSER_H
#define PARSER_H

extern char *delims;

bool AddTokenToTree(Tree *tree, Tokenizer *tokenizer);
Tree *GetParseTree(char *command);
char *trimws(char *str);

#endif //PARSER_H
