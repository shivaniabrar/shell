//
// Created by Abrar Shivani on 2/27/17.
//

//
// Created by Abrar Shivani on 2/26/17.
//

#include <stdio.h>
#include "parser.h"
#include "tree.h"
#include "framework_api.h"
#include "framework.h"
// test name
char *testName;

/* test output files */
#define TESTPF "test_pagefile.bin"

/* prototypes for test functions */
static void testParser(char *command, char *expected);

int
main(int argc, char **argv) {

    int i = 0;
    testName = "";
    size_t ntestcases = 0;
    char *testcases[][2] = {
            {"a & b & c", " PARA a b c"},
            {"a ; b ; c", " SEQ a b c"},
            {"a & (b ; c)", " PARA a SEQ b c" },
            {"(a & b) ; c", " SEQ PARA a b c"},
            {";c", " SEQ c"},
            {"&c", " PARA c"},
            {"(a)", " NOOP NOOP a"},
            {"(((a;)))", " NOOP NOOP NOOP SEQ a"},
            {"(((a;))&b)", " NOOP PARA NOOP SEQ a b"},
            {"(((a;))b)", ""},
    };

    ntestcases = sizeof(testcases) / sizeof(char *) / 2;
    printf("%d", ntestcases);
    for (i = 0; i < ntestcases; i++) {
        testParser(testcases[i][0], testcases[i][1]);
    }
    TEST_DONE();
    return 0;
}

void
testParser(char *command, char *expected)
{
    Tree *tree = NULL;
    char *result = NULL;
    char commandTestName[100];
    sprintf(commandTestName,"command: \"%s\"", command);
    testName = commandTestName;
    tree = GetParseTree(command);
    result = PrintTreeToString(tree);
    ASSERT_EQUALS_STRING(expected, result, "Parse result verification");
    free(result);
    DestroyTree(tree);
}

