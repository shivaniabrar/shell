//
// Created by Abrar Shivani on 2/25/17.
//

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <termios.h>

#include "evaluator.h"
#include "tokenizer.h"
#include "tree.h"
#include "common.h"
#include "shell.h"
#include "parser.h"
#include "history.h"

#define OFF 0
#define ON 1
#define USER_INPUT_BUFFER_SIZE 100
#define PATH_SIZE 100

char message[PATH_SIZE];

bool parse_and_execute(char *buffer);
int get_command_from_user(History *history, char *buffer);
int write_backspace(char *buffer, int position);
int clear_and_rewrite_buffer(char *buffer);
int display_prompt_message();
char *get_prompt_message();
int clear_screen(int position);
bool read_and_execute_profile();
void sigint_handler(int sig);
char *get_prompt_message();

int
main(int argc, char **argv)
{
    char buffer[USER_INPUT_BUFFER_SIZE];
    History history;
    bool result = true;
    /* Install the signal handlers */
    Signal(SIGINT,  sigint_handler);
    Signal(SIGQUIT,  sigquit_handler);
    Signal(SIGALRM, sigalrm_handler);

    setenv("PATH", "", 1);
    setenv("HOME", "", 1);

    if (read_and_execute_profile() == false) {
        printf("Warning: Profile file is not initialised\n");
        printf("You can set path and home variable using command set\n");
    } else {
        if (getenv("PATH") == NULL || strcmp(getenv("PATH"),"") == 0) {
            printf("Warning: PATH not set\n");
            printf("You can set PATH variable using command set\n");
        }
        if (getenv("HOME") == NULL || strcmp(getenv("HOME"),"") == 0) {
            printf("Warning: HOME not set\n");
            printf("You can set HOME variable using command set\n");
        }
    }


    result = InitHistory(&history, "history_of_commands");
    if (result == false) {
        CloseHistory(&history);
        printf("Error while initializing history");
        exit(0);
    }

    while(1) {
        bzero(buffer, sizeof(char) * USER_INPUT_BUFFER_SIZE);
        get_command_from_user(&history, buffer);
        if (strlen(buffer) == 0) {
            continue;
        }
        AddToHistory(&history, buffer);
        parse_and_execute(buffer);
    }

    return 0;
}

bool
read_and_execute_profile() {
    FILE *fp = NULL;
    int line = MAX_CHAR_FOR_CMD_LINE;
    char buffer[USER_INPUT_BUFFER_SIZE];
    fp= fopen(PFOFILE_FILE_NAME, "r");

    if (fp == NULL) {
        perror("File ('.profile')");
        return false;
    }

    bzero(buffer, sizeof(char) * USER_INPUT_BUFFER_SIZE);

    while (fgets(buffer, line, fp) != NULL) {
        parse_and_execute(buffer);
        bzero(buffer, sizeof(char) * USER_INPUT_BUFFER_SIZE);
    }

    return true;

}

bool
parse_and_execute(char *buffer)
{
    char *input = NULL;
    Tree *tree = NULL;
    input = strdup(buffer);
    if (execute(input, true) == 1) {
        free(input);
        return true;
    }
    tree = GetParseTree(input);
#ifdef DEBUG
    PrintTree(tree);
#endif
    if (tree == NULL) {
        printf("Syntax error\n");
        return false;
    }
    Evaluator(tree);
    DestroyTree(tree);
    free(input);
    return true;
}

int
get_command_from_user(History *history, char *buffer)
{
    Token *token = NULL;
    int arrow_keys = OFF;
    char usr_response = '\0';
    char bell = '\a';
    int buffer_position = 0;
    struct termios old,new;
    display_prompt_message();
    fflush(stdout);
    tcgetattr(0,&old);
    new = old;
    new.c_lflag&=~ICANON;
    new.c_lflag&=~ECHO;
    tcsetattr(0,TCSANOW,&new);


    while (read(0, &usr_response, 1), usr_response != '\n') {

        if (iscntrl((int)usr_response)) {
            switch ((int)(usr_response)) {
                case 9:
                    if (token != NULL) {
                        FreeToken(token);
                    }
                    token = AutoComplete(history, buffer);
                    if (token != NULL) {
                        clear_screen(buffer_position);
                        buffer[++buffer_position] = '\0';
                        GetCmdFromToken(token, buffer);
                        buffer_position = clear_and_rewrite_buffer(buffer);
                    }
                    continue;
                case 127:
                case 8:
                    // handle backspace
                    if (buffer_position == 0) {
                        continue;
                    }
                    /*
                    buffer_position -= 2;
                    usr_response = '\b';
                    write(1,&usr_response, 1);
                    usr_response = ' ';
                    write(1,&usr_response, 1);
                    usr_response = '\b';
                    break;
                    */
                    buffer_position = write_backspace(buffer, buffer_position);
                    continue;
                default:
                    continue;
            }
        }
        switch (usr_response) {
            case 91:
                arrow_keys = ON;
                continue;
            case 65:
                if (arrow_keys == OFF) {
                    break;
                } else {
                    if (token != NULL) {
                        if (SearchUp(token) == true) {
                            clear_screen(buffer_position);
                            GetCmdFromToken(token, buffer);
                            buffer_position = clear_and_rewrite_buffer(buffer);
                        } else {
                            write(1, &bell, sizeof(bell));
                        }
                    }
                    arrow_keys = OFF;
                    continue;
                } // Up Key Pressed
            case 66:
                if (arrow_keys == OFF) {
                    break;
                } else {
                    if (token != NULL) {
                        if (SearchDown(token) == true) {
                            clear_screen(buffer_position);
                            GetCmdFromToken(token, buffer);
                            buffer_position = clear_and_rewrite_buffer(buffer);
                        } else {
                            write(1, &bell, sizeof(bell));
                        }
                    }
                    arrow_keys = OFF;
                    continue;
                } // Down Key Pressed
            case 67:
            case 68:
                if (arrow_keys == ON) {
                    //write(1,&usr_response, 1);
                    arrow_keys = OFF;
                    continue;
                }
                break;
        }
        write(1,&usr_response, 1);
        buffer[buffer_position] = usr_response;
        //arrow_keys = ON;
        ++buffer_position;
    }
    printf("\n");
    buffer[buffer_position] = '\0';
    tcsetattr(0,TCSANOW,&old);
    if (token != NULL) {
        FreeToken(token);
    }
    return 0;
}

int
write_backspace(char *buffer, int position)
{
    char carriage_return = '\r';
    char backspace = '\b';
    buffer[position-1] = ' ';
    write(1,&carriage_return, sizeof(char));
    display_prompt_message();
    fflush(stdout);
    write(1, buffer, (position) * sizeof(char));
    write(1, &backspace, sizeof(char));
    return --position;
}

int
clear_screen(int position)
{
    char carriage_return = '\r';
    char space = ' ';
    position += strlen(get_prompt_message());
    write(1,&carriage_return, sizeof(char));
    while(position >= 0) {
        write(1, &space, sizeof(char));
        --position;
    }
    write(1,&carriage_return, sizeof(char));
    return 0;
}

int
clear_and_rewrite_buffer(char *buffer)
{
    char carriage_return = '\r';
    int position = 0;
    if (buffer == NULL){
        return 0;
    }
    position = strlen(buffer);
    write(1,&carriage_return, sizeof(char));
    display_prompt_message();
    fflush(stdout);
    write(1, buffer, (position) * sizeof(char));
    //write(1, &backspace, sizeof(char));
    return position;
}

char *
get_prompt_message()
{
    if (getcwd(message, sizeof(message)) != NULL) {
        strcat(message, ">");
        return message;
    } else {
        return "shell>";
    }
}

int
display_prompt_message()
{
    char *prompt_message = get_prompt_message();
    printf("%s", prompt_message);
    return 0;
}
