//
// Created by Abrar Shivani on 2/23/17.
//

#include <stdlib.h>
#include <string.h>
#include "tokenizer.h"

Tokenizer *
CreateTokenizer(char *string)
{
    Tokenizer *tokenizer = NULL;
    if (string == NULL) {
        return NULL;
    }
    tokenizer = malloc(sizeof(Tokenizer));
    bzero(tokenizer, sizeof(tokenizer));
    tokenizer->reference = strdup(string);
    tokenizer->start = tokenizer->reference;
    return tokenizer;
}

Tokenizer *
GetNextToken(Tokenizer *tokenizer, char *delims)
{
    char *start = NULL;
    char *delim = NULL;

    if (tokenizer == NULL || tokenizer->start == NULL || delims == NULL) {
        return NULL;
    }

    if (*tokenizer->start == '\0') {
        return NULL;
    }

    // Store state
    start = tokenizer->start;
    tokenizer->delim = '\0';

    while(*tokenizer->start != '\0') {
        for(delim = delims; *delim != '\0' && *delim != *tokenizer->start; ++delim);
        if (*delim == '\0') {
            ++tokenizer->start;
            continue;
        }
        tokenizer->delim = *delim;
        tokenizer->token = start;
        *tokenizer->start = '\0';
        ++tokenizer->start;
        return tokenizer;
    }
    tokenizer->token = start;
    return tokenizer;
}

bool
DestroyTokenizer(Tokenizer *tokenizer)
{
    if (tokenizer == NULL) {
        return false;
    }
    free(tokenizer->reference);
    free(tokenizer);
    return true;
}