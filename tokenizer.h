//
// Created by Abrar Shivani on 2/23/17.
//

#include "common.h"
#ifndef TOKENIZER_H
#define TOKENIZER_H

typedef struct Tokenizer {
    char *token;
    char *start;
    char *reference;
    char  delim;
} Tokenizer;

Tokenizer *CreateTokenizer(char *string);
Tokenizer *GetNextToken(Tokenizer *tokenizer, char *delims);
bool DestroyTokenizer(Tokenizer *tokenizer);

#endif //TOKENIZER_H
