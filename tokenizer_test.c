//
// Created by Abrar Shivani on 2/27/17.
//

//
// Created by Abrar Shivani on 2/26/17.
//

#include <stdio.h>
#include "tokenizer.h"
#include "framework_api.h"
#include "framework.h"

// test name
char *testName;

/* test output files */
#define TESTPF "test_pagefile.bin"

/* prototypes for test functions */
static void testTokenizer(char *command, char *expected);

int
main(int argc, char **argv) {

    int i = 0;
    testName = "";
    size_t ntestcases = 0;

    char *testcases[][2] = {
            {"(",                          " (<D>: <T>"},
            {")",                          " )<D>: <T>"},
            {";",                          " ;<D>: <T>"},
            {"&",                          " &<D>: <T>"},
            {";;;",                        " ;<D>: <T> ;<D>: <T> ;<D>: <T>"},
            {"&&&",                        " &<D>: <T> &<D>: <T> &<D>: <T>"},
            {"((())",                      " (<D>: <T> (<D>: <T> (<D>: <T> )<D>: <T> )<D>: <T>"},
            {"c ; (a&b)",                  " ;<D>: c <T> (<D>:  <T> &<D>: a<T> )<D>: b<T>"},
            {"(a;)",                       " (<D>: <T> ;<D>: a<T> )<D>: <T>"},
            {"cmd1 arg1 -t amd(cmd2)cmd3", " (<D>: cmd1 arg1 -t amd<T> )<D>: cmd2<T>  <D>: cmd3<T>"},
    };

    ntestcases = sizeof(testcases) / sizeof(char *) / 2;
    printf("%d", ntestcases);
    for (i = 0; i < ntestcases; i++) {
        testTokenizer(testcases[i][0], testcases[i][1]);
    }
    TEST_DONE();
    return 0;
}

void
testTokenizer(char *command, char *expected) {
    char result[200];
    char *delims = ";&()";
    char commandTestName[100];
    Tokenizer *tokenizer = NULL;
    bzero(result, sizeof(result));
    sprintf(commandTestName,"command: \"%s\"", command);
    testName = commandTestName;
    tokenizer = CreateTokenizer(command);
    while (GetNextToken(tokenizer, delims) != NULL) {
        if (tokenizer->delim == '\0') {
            sprintf(result, "%s %s<D>: %s<T>", result, " ", tokenizer->token);
        } else {
            sprintf(result, "%s %c<D>: %s<T>", result, tokenizer->delim, tokenizer->token);
        }
    }
    ASSERT_EQUALS_STRING(expected, result, "Tokenizer result verification");
    DestroyTokenizer(tokenizer);
}

