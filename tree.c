//
// Created by Abrar Shivani on 2/23/17.
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <strings.h>
#include "tree.h"
#include "shell.h"

Node *
CreateNode(Operator operator, char *value) {
    Node *node = NULL;
    if (operator == COMMAND && value == NULL) {
        return NULL;
    }
    node = malloc(sizeof(Node));
    node->operator = operator;
    node->parent = NULL;
    if (operator == COMMAND) {
        node->value = strdup(value);
    } else {
        node->value = NULL;
    }
    InitListNode(&node->child);
    InitListNode(&node->sibling);
    InitListNode(&node->all_nodes);
    node->number_of_child = 0;
    node->pid = -1;
    return node;
}

Node *
ChangeNOOPToOtherOperator(Node *node, Operator operator) {
    if (node == NULL || operator == NOOP || operator == COMMAND) {
        return node;
    }
    node->operator = operator;
    return node;
}

bool
AddChild(Node *node, Node *child) {
    if (node == NULL || child == NULL) {
        return false;
    }
    AddToBack(&node->child, &child->sibling);
    ++node->number_of_child;
    child->parent = node;

    return true;
}

Tree *
CreateTree() {
    Tree *tree = malloc(sizeof(Tree));
    tree->number_of_nodes = 0;
    tree->parent = tree->root = CreateNode(NOOP, NULL);
    if (tree->root == NULL) {
        free(tree);
        return NULL;
    }
    InitListNode(&tree->all_nodes);
    ++tree->number_of_nodes;
    return tree;
}

bool
AddNodeToTree(Tree *tree, Node *node) {
    if (tree == NULL || node == NULL) {
        return false;
    }

    AddToBack(&tree->all_nodes, &node->all_nodes);
    ++tree->number_of_nodes;
    return true;
}

Node *
CheckAndAddChild(Tree *tree, Node *child) {
    bool result = false;
    if (tree == NULL || child == NULL || tree->parent == NULL) {
        return NULL;
    }

    if (child->parent != NULL) {
        return NULL;
    } // Cannot add child to multiple parents;

    if (tree->parent->operator == NOOP && tree->parent->number_of_child > 0) {
        return NULL;
    } // Parent can have only one noop child

    result = AddChild(tree->parent, child);
    if (result == false) {
        return NULL;
    }

    result = AddNodeToTree(tree, child);
    if (result == false) {
        return NULL;
    }

    return child;
}

Node *
CheckAndCreateChild(Tree *tree, Operator operator, char *value) {
    Node *child = NULL;
    child = CreateNode(operator, value);
    if (child == NULL) {
        return NULL;
    }
    if (CheckAndAddChild(tree, child) == NULL) {
        DestroyNode(child);
        return NULL;
    }
    return child;
}

Node *
GetRoot(Tree *tree) {
    if (tree == NULL) {
        return NULL;
    }
    return tree->root;
}


bool
SetRoot(Tree *tree, Node *root) {
    if (tree == NULL || root == NULL) {
        return false;
    } // Root cannot be NULL
    tree->root = root;
    return true;
}

Node *
GetParent(Tree *tree) {
    if (tree == NULL) {
        return NULL;
    }
    return tree->parent;
}

bool
SetParent(Tree *tree, Node *parent) {
    if (tree == NULL || parent == NULL) {
        return false;
    } // Parent cannot be NULL
    tree->parent = parent;
    return true;
}

bool
DestroyNode(Node *node) {
    if (node == NULL) {
        return false;
    }
    if (node->value != NULL) {
        free(node->value);
    } // Created using strdup

    RemoveFromList(&node->all_nodes);
    RemoveFromList(&node->child);
    RemoveFromList(&node->sibling);

    free(node);
    return true;
}

bool
DestroyTree(Tree *tree) {
    Node *next;
    Node *prev;
    if (tree == NULL) {
        return false;
    }
    if (tree->root != NULL) {
        next = prev = tree->root;
        FOR_EACH(Node, all_nodes, (&tree->all_nodes), next)
            if (prev != tree->root) {
                DestroyNode(prev);
            }
            prev = next;
        }
        DestroyNode(prev);
        if (tree->root != prev) {
            DestroyNode(tree->root);
        }
        tree->root = NULL;
    }
    tree->parent = NULL;
    tree->number_of_nodes = 0;
    free(tree);
    return true;
}

int
PrintTree(Tree *tree)
{
    Node *root = NULL;
    if (tree == NULL) {
        return 0;
    }
    root = GetRoot(tree);
    return _print_tree(root);
}

bool
_print_tree(Node *root) {
    Node *node_child = NULL;
    if (root == NULL) {
        return false;
    }

    if (root->operator == SEQUENTIAL) {
        LOG("SEQ\n");
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            _print_tree(node_child);
        }
    } else if (root->operator == PARALLEL) {
        LOG("PARA\n");
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            _print_tree(node_child);
        }
    } else if (root->operator == NOOP) {
        LOG("NOOP\n");
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            _print_tree(node_child);
        }

    } else if (root->operator == COMMAND) {
        LOG("%s\n", root->value);
    } else {
        LOG("Unknown Operator: %u\n", root->operator);
    }

    return true;
}

char *
PrintTreeToString(Tree *tree)
{
    Node *root = NULL;
    char *string = NULL;
    string = malloc(sizeof(string) * 100);
    bzero(string, sizeof(string));
    if (tree == NULL) {
        return string;
    }
    root = GetRoot(tree);
    _print_tree_to_string(root, string);
    return string;
}

bool
_print_tree_to_string(Node *root, char *string) {
    Node *node_child = NULL;
    if (root == NULL) {
        return false;
    }

    if (root->operator == SEQUENTIAL) {
        sprintf(string, "%s SEQ", string);
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            _print_tree_to_string(node_child, string);
        }
    } else if (root->operator == PARALLEL) {
        sprintf(string, "%s PARA", string);
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            _print_tree_to_string(node_child, string);
        }
    } else if (root->operator == NOOP) {
        sprintf(string, "%s NOOP", string);
        FOR_EACH(Node, sibling, (&(root->child)), node_child)
            _print_tree_to_string(node_child, string);
        }

    } else if (root->operator == COMMAND) {
        sprintf(string, "%s %s", string, root->value);
    } else {
        sprintf(string, "INVAL ");
    }

    return true;
}