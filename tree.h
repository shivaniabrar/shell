//
// Created by Abrar Shivani on 2/23/17.
//

#include "list.h"
#include  "common.h"
#include <unistd.h>
#include <sys/types.h>

#ifndef TREE_H
#define TREE_H

typedef enum Operator {
    NOOP,
    PARALLEL,
    SEQUENTIAL,
    COMMAND,
} Operator;

typedef struct Node {
    Operator operator;
    char *value;
    char **argv;
    pid_t pid;
    struct Node *parent;
    ListNode child;
    ListNode sibling;
    ListNode all_nodes;
    unsigned int number_of_child;
} Node;

typedef struct Tree {
    Node *root;
    Node *parent;
    ListNode all_nodes;
    unsigned int number_of_nodes;
} Tree;

Tree *CreateTree();
Node *GetRoot(Tree *tree);
bool SetRoot(Tree *tree, Node *root);
Node *GetParent(Tree *tree);
bool SetParent(Tree *tree, Node *parent);
bool AddNodeToTree(Tree *tree, Node *node);
Node *CheckAndAddChild(Tree *tree, Node *child);
Node *CheckAndCreateChild(Tree *tree, Operator operator, char *value);

Node *CreateNode(Operator operator, char *value);
Node *ChangeNOOPToOtherOperator(Node *node, Operator operator);

bool AddChild(Node *node, Node *child);

int PrintTree(Tree *tree);
bool _print_tree(Node *root);

char * PrintTreeToString(Tree *tree);
bool _print_tree_to_string(Node *root, char *string);

bool DestroyNode(Node *node);
bool DestroyTree(Tree *tree);


#endif //TREE_H
